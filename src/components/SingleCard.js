import './SingleCard.css'


export default function SingleCard({ card, handleChoice, flipped, disabled }) {


    const handleClick = () => {
        if (!disabled) {
            handleChoice(card)
        }
    }



    return (
        <div className='card' id="inFunc">
            <div className={flipped ? "flipped" : ""}>
                <img className='front' alt='card front' src={card.src}></img>
                <img className='back' src='/img/cover.png' alt='card back' onClick={handleClick}></img>
            </div>
        </div>
    )
}